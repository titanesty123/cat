import BaseApiService from '~/services/BaseApiService';

export default class CatsApiService extends BaseApiService {
  async getCats() {
    try {
      return await this.axios.$get(`/images/search`, {
        headers: {
          'x-api-key': process.env.apiKey
        },
        params: {
          limit: 15,
          size : 'thumb'
        }
      });
    } catch (error) {
      alert(error);
    }
  }
}
